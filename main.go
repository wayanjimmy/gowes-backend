package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	config "github.com/spf13/viper"
)

func isMigrate() bool {
	return len(os.Args) == 2 && os.Args[1] == "--migrate"
}

func init() {
	configure()
}

func main() {
	if isMigrate() {
		up()
		fmt.Println("migrated")
	} else {
		router := newRouter()
		log.Fatal(http.ListenAndServe(config.GetString("host"), router))
	}
}
