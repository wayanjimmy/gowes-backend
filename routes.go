package main

import (
	"net/http"

	"github.com/VividCortex/siesta"
)

type route struct {
	Verb        string
	UriPath     string
	Usage       string
	HandlerFunc http.HandlerFunc
}

type routes []route

var rs = routes{
	route{
		"POST",
		"/auth/register",
		"User register",
		registerUserHandler,
	},
	route{
		"POST",
		"/auth/login",
		"User login",
		loginUserHandler,
	},
}

func newRouter() *siesta.Service {
	service := siesta.NewService("/")

	for _, r := range rs {
		service.Route(r.Verb, r.UriPath, r.Usage, r.HandlerFunc)
	}

	return service
}
