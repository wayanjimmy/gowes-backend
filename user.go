package main

import (
	"time"
)

type userWithPassword struct {
	Name     string
	Email    string
	Password string
}

type user struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	Email     string    `json:"email" sql:"unique"`
	Name      string    `json:"name"`
	Password  string    `json:"-"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
