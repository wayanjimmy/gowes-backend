package main

func up() {
	db, _ := getDB()

	db.DropTableIfExists(&user{})
	db.DropTableIfExists(&location{})

	db.Set("gorm:table_options", "ENGINE=InnoDB")
	db.AutoMigrate(&user{}, &location{})
}
