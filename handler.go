package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

func registerUserHandler(w http.ResponseWriter, r *http.Request) {
	db, _ := getDB()
	var input userWithPassword
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &input); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	password := []byte(input.Password)
	hashedPassword, _ := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)

	u := user{
		Name:     input.Name,
		Email:    input.Email,
		Password: string(hashedPassword),
	}
	db.Create(&u)

	if u.ID != 0 {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(u); err != nil {
			panic(err)
		}
	} else {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(&errorResponse{Message: "User already exist"}); err != nil {
			panic(err)
		}
	}
}

func loginUserHandler(w http.ResponseWriter, r *http.Request) {
	db, _ := getDB()
	db.DB()
	var input userWithPassword
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &input); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	var findUser user

	u := user{
		Email: input.Email,
	}
	db.First(&findUser, u)

	errPassword := bcrypt.CompareHashAndPassword([]byte(findUser.Password), []byte(input.Password))

	if findUser.ID != 0 && errPassword == nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
		if err := json.NewEncoder(w).Encode(&findUser); err != nil {
			panic(err)
		}
	} else {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(&errorResponse{Message: "Email/password invalid"}); err != nil {
			panic(err)
		}
	}
}
