package main

import (
	"fmt"
	"os"
	"strings"

	config "github.com/spf13/viper"
)

const (
	KEY_ENV         = "GOWES_DIR"
	CONFIG_FILENAME = "config.json"
)

func configure() {
	dir := os.Getenv(KEY_ENV)
	if strings.TrimSpace(dir) == "" {
		message := fmt.Sprintf("%s is not defined", KEY_ENV)
		fmt.Println(message)
		os.Exit(1)
	}

	dir = strings.TrimRight(dir, "/")
	config.SetConfigFile(fmt.Sprintf("%s/%s", dir, CONFIG_FILENAME))
	if err := config.ReadInConfig(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	config.Set("dir", dir)
}
