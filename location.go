package main

import (
	"time"
)

type location struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	UserId    uint      `json:"user_id"`
	Latitude  float32   `json:"latitude"`
	Longitude float32   `json:"longitude"`
	Speed     float32   `json:"speed"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
