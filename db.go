package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	config "github.com/spf13/viper"
)

func getDB() (gorm.DB, error) {
	return gorm.Open("mysql", config.GetString("database"))
}
